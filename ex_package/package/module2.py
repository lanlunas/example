import numpy as np


def matrix_by_three(alist):
    if not isinstance(alist, list):
        raise Exception("matrix_by_three() only accept 'list' as argument!!!")
    return np.multiply(alist, 3)


if __name__ == "__main__":
    print(matrix_by_three([1, 2, 3]))
