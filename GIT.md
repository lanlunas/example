# Getting Started #
## 1) Download git
https://git-scm.com/downloads

## 2) Setup up Bit-Bucket
### Login into your bit-bucket account and setup ssh
https://bitbucket.org/account/settings/ssh-keys/

### Follow instruction to setup ssh key
https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/

### Clone Repos 
Open gitbash in the folder that you want to save the repos run the clone 
using the url provided by bitbucket. For example.

    git clone git@bitbucket.org:lanlunas/example.git
    
### Checkout remote branch
    git branch
    
You should see there is at least a branch name master

### Create a my_code branch from master
    git checkout -b my_code
This will copy all the code in master branch to my_code branch
### Check the branch again 
    git branch
You should see 2 branches `master` and `my_code` branches

## 3) Daily routine work
Assuming you are in `my_code` branch and there is nothing to commit

### Go to local master branch
    git checkout master
### Pull code from remote 
    git pull
### Now you local master branch code is the same at the remote master branch
We need to update `my_code` with the change make by other people
### Checkout 'my_code'
    git checkout my_code
### Merge local master with my_code
    git merge master

### 
 
 
# Tutorials #
## Cheat Sheet
https://education.github.com/git-cheat-sheet-education.pdf
## Detail Video
https://www.youtube.com/watch?v=RGOj5yH7evk

# STEPS: ADD - COMMIT - PUSH

## FILE ##
All of file's actions happen on local machine
A file should be created, changed, deleted and moved in PyCharm.

#### Add all changes in a directory (recursively) ####
```
    git add path/to/directory
```
#### ADD a file ####
Only affect locally
```
    git add path/to/file
```
### Compare changes since last add ###
```
    git diff
    git diff path/to/file
```

## COMMIT ##
Only affect locally. A commit can include many files added, deleted, moved.
```
    git commit -m "what this commit does"
```
## PUSH ##
Many changes are included in a push.
All changes will be submitted to server and cannot be changed, all reverts will be recorded in history.
```
    git push origin branch_name
```

## BRANCH ##
#### Create a new branch ####
```
    git checkout -b branch_name
```
#### Checkout a branch from server ####
```
    git checkout branch_name
```
#### Update the current branch ####
```
    git pull
```
#### Check status of a branch ####
```
    git status
```
This command lists all files that haven't been added, commited; 
how many commits haven't been submitted.
#### Check all commits ####
```
    git log
```
This command shows all commits that have been downloaded to or created in the local machine

#### List all local branches ###
```
    git branch
``` 
#### list all branches ####
```
    git branch -a
```
#### Delete a branch locally ####
```
    git branch -d local_branch
```

## RESET ##
#### Reset a commit in local machine ###
Giving the following sittuation:

* Point 0: at previous commit
* Point 1: change some files
* Point 2: add changes
* Point 3: commit changes

```
    git reset --soft HEAD^   (return to Point 2 to change commit message)
    git reset --mixed HEAD^   (return to Point 1 re-add before commit)
    git reset --soft HEAD^   (return to Point 0, no changes since previous commit are kept) 
```
#### Reset a file to be the same as the beginning of the branch ###
```
    git checkout HEAD -- my-file.txt
```
#### Reset the whole branch to be as the current state in the server ####
```
    git reset --hard origin/branch_name
```

## MERGE a branch to master ##
If the pipeline is set, branch must passed all checking in pipeline like flake8, unittest.
All updates in master branch must be merge into the current branch by:
```
    git checkout master
    git pull
    git checkout current_branch
    git merge master
```
After merging with master, some files may be marked as conflicted in the code. There may be different conflict sections in one file.
```
    <<<<<<<<< HEAD
    current_branch's code
    ==========
    master's code
    >>>>>>>>> origin/master
```
The developer must solve the conflict sections and removes the conflict mark, add, merge, push to be able to merge file into master.