# Getting Started #
## ANACONDA ##
Install anaconda to create different python environments for different projects:

https://docs.anaconda.com/anaconda/install/

Commands *(just in case)*:

https://docs.conda.io/projects/conda/en/latest/commands.html

#### If 'command not found' showed when running conda in console ####
```
    echo ". /c/ProgramData/Anaconda3/etc/profile.d/conda.sh" >> ~/.profile
```
#### Update ####
```
    conda update conda --all     
```
*(run as administrator in anaconda promt)*
#### Create Environment 'al' ####
```
    conda create -n al python=3.7 anaconda
```
#### Start Evironment 'al' ####
```
    conda activate al
```

## Prerequisites ##
Install the dependencies in requirements.txt using pip install
```
    $ pip install -r requirements.txt
```
## PEP8 convention ##
This convention helps you to have clean code.
```
    $ flake8 path/to/files
```
## Unittest ##
Developers need to write unittests to test most of functions and workflow to make sure they work as intended.

When code is abundant, it is hard to check if all functions and workflow are still working when a developer change a piece of code. Running unittest will help to ensure that.

Unittest filename:
```
    test_[file_to-test].py
```
Unittest file are required to be created under `test/` directory with the same structure as the package of the file that it is testing.

#### Run all tests together ####
```
    $ python tests/all_tests.py
```
#### Run an individual test ####
```
    $ python -m path.to.module
```
*( module is test's filename without '.py')*

#### Coverage ####
To check how much code unittest cover
```
    $ coverage run tests/all_tests.py
    $ coverage report
OR  $ coverage html
```
For the last command, it will create a directory called `htmlcov/` 

Open `html/index.html` to check for what the tests covered or not covered.


