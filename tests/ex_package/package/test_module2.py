'''
    unittest for ex_package/module1/ex1_for_unittest.py
    testing command: python -m tests.ex_package.module1.test_module2
'''
import unittest
from ex_package.package import module2


class TestModule2(unittest.TestCase):
    def test_matrix_by_three(self):
        # test wrong type of argument
        with self.assertRaises(Exception) as context:
            module2.matrix_by_three(2)
            self.assertEqual(context.exception.message,
                             "matrix_by_three() only accept 'list' as argument!!!")

        # test correct type of argument
            ret = module2.matrix_by_three([3,5,9])
            self.assertEqual([9, 15, 27])

if __name__ == "__main__":
    unittest.main()