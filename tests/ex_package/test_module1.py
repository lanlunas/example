'''
    unittest for ex_package/ex_for_unittest.py
    testing command: python -m tests.ex_package.test_module1
'''

import unittest
from ex_package import module1

class TestModule1(unittest.TestCase):
    def test_hello_world(self):
        s = "NAME"
        ret = module1.hello_world(s)
        self.assertEqual(ret, "Hello %s." % s)

    def test_time_by_two(self):
        a = 6
        ret = module1.time_by_two(a)
        self.assertEqual(ret, 12)

if __name__ == "__main__":
    unittest.main()